from aiohttp import web

routes = web.RouteTableDef()


@routes.get('/')
async def hello(request):
    return web.Response(status=200, text="Hello There!")


def main():

    app = web.Application()

    app.add_routes(routes)

    web.run_app(app, port=8080)


if __name__ == '__main__':
    main()
