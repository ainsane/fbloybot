import gc
from aiohttp import web, ClientSession
import logging
import handlers
import fbAPI
import loyAPI
import static
import asyncmongo
from asyncio import get_event_loop

routes = web.RouteTableDef()

logger = logging.getLogger('FBloylogger')
logger.setLevel(logging.ERROR)

loop = get_event_loop()

bot = fbAPI.Bot(static.PAGE_ACCESS_TOKEN, loop)
loyalty = loyAPI.LoyAPI(loop)
mongo = asyncmongo.MongoWorker()


@routes.get('/')
async def hello(request):
    data = request.query

    if data.get('hub.mode') == 'subscribe' and data.get('hub.challenge'):
        if not data.get('hub.verify_token') == 'himarker':
            return 'Verification.token.mismatch', 403
        return web.Response(text=data['hub.challenge'], status=200)
    return web.Response(status=200, text='ok')


@routes.post('/')
async def post_handler(request):
    try:
        data = await request.json()
        if data['object'] == 'page':
            for entry in data['entry']:
                for message_event in entry['messaging']:

                    if message_event.get('message') and not message_event['message'].get('quick_reply'):
                        await handlers.handle_simple_message(message_event)

                    elif message_event.get('message') and message_event['message'].get('quick_reply'):
                        await handlers.handle_quick_reply(message_event)

                    elif message_event.get('postback'):
                        await handlers.handle_postbacks(message_event)

        return web.Response(status=200)

    except Exception as e:
        logger.exception(e)
        return web.Response(status=200)


async def on_shutdown(app):
    for obj in gc.get_objects():
        if isinstance(obj, ClientSession):
            await obj.close()
#    await app['bot'].close()
#    await app['loy'].close()


def main():

    app = web.Application()

    app.add_routes(routes)

    logging.basicConfig(level=logging.DEBUG)

    app['bot'] = bot
    app['loy'] = loyalty

    app.on_shutdown.append(on_shutdown)

    web.run_app(app, port=8085)


if __name__ == '__main__':
    main()
