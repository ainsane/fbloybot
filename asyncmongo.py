import motor.motor_asyncio
import loggs


logger = loggs.errorlog


class MongoWorker:
    def __init__(self):
        self.client = motor.motor_asyncio.AsyncIOMotorClient('localhost', 27017)
        self.db = self.client.syncmongo
        self.coll = self.db.users

#    async def close(self):
 #       await self.client.close()

    async def write_user(self, str_sender_id, state):
        try:
            data = {
                '_id': str_sender_id,
                'state': state
            }
            await self.coll.insert_one(data)
            return True

        except Exception as e:
            logger.exception(e)
            return False

    async def write_phone(self, str_sender_id, phone, state):
        try:
            data = {
                'phone': phone,
                'state': state
            }
            update = await self.coll.update_one({'_id': str_sender_id}, {'$set': data})
            if update.modified_count == 1:
                return True
            else:
                return False
        except Exception as e:
            logger.exception(e)
            return False

    async def find_user(self, str_sender_id):
        try:
            data = await self.coll.find_one({'_id': str_sender_id})
            if data is None:
                return False
            else:
                return data
        except Exception as e:
            logger.exception(e)

    async def write_field(self, str_sender_id, **kwargs):
        d = {}
        for key, val in kwargs.items():
            x = {key: val}
            d = {**d, **x}
        try:
            update = await self.coll.update_one({'_id': str_sender_id}, {'$set': d})
            if update.modified_count == 1:
                return True
            else:
                return False
        except Exception as e:
            logger.exception(e)

    async def set_state(self, str_sender_id, state):
        try:
            update = await self.coll.update_one({'_id': str_sender_id}, {'$set': {'state': state}})
            if update.modified_count == 1:
                return True
            else:
                return False
        except Exception as e:
            logger.exception(e)




# Проверочка походу

#loop = asyncio.get_event_loop()
#cls = MongoWorker()
#mtd = cls.write_user('123456', 'GOOOODDD')
#dic = {'field4': 'new4444'}
#run = loop.run_until_complete(cls.set_state("123456", 'state'))
#print(run)
