
import aiohttp
import static
import json
import loggs

error_log = loggs.errorlog
info_log = loggs.infolog


class Bot:

    def __init__(self, access_token, loop):
        self.graph_url = f'https://graph.facebook.com/v2.6/me/messages?access_token={access_token}'
        self.session = aiohttp.ClientSession(loop=loop)

    async def close(self):
        info_log.info(self.session)
        await self.session.close()
        


    async def send_text_message(self, sender_id, message):
        try:
            data = {"recipient": {"id": str(sender_id)},
                    "message": {"text": message}}

            async with self.session.post(self.graph_url, data=json.dumps(data),
                                         headers={'Content-Type': 'Application/json'}) as response:
                if response.status != 200:
                    info_log.info(response.text)
        except Exception as e:
            error_log.exception(e)


    async def send_quick_reply_text(self, sender_id, text_message, **kwargs):
        try:
            butt = []
            for key, value in kwargs.items():
                if not key.startswith('е'):
                    listres = {
                        "content_type": "text",
                        "title": f"{key}",
                        "payload": f"{value}"
                    }
                    butt.append(listres)

            for key, value in kwargs.items():
                if key.startswith('е'):
                    listres = {
                        "content_type": "text",
                        "title": f"{key}",
                        "payload": f"{value}"
                    }
                    butt.append(listres)

            data = {
                "messaging_type": "RESPONSE",
                "recipient": {"id": f"{sender_id}"},
                "message": {
                    "text": f"{text_message}",
                    "quick_replies": butt}
            }

            async with self.session.post(self.graph_url, data=json.dumps(data),
                                         headers={'Content-Type': 'Application/json'}) as response:
                if response.status != 200:
                    info_log.info(response.text)
        except Exception as e:
            error_log.exception(e)

    async def quick_reply_phone(self, sender_id, text_message):
        try:
            data = {
                "messaging_type": "RESPONSE",
                "recipient": {"id": f"{sender_id}"},
                "message": {
                    "text": f"{text_message}",
                    "quick_replies": [
                        {"content_type": "user_phone_number"}
                    ]}}
            async with self.session.post(self.graph_url, data=json.dumps(data),
                                         headers={'Content-Type': 'Application/json'}) as response:
                if response.status != 200:
                    info_log.info(response.text)

        except Exception as e:
            error_log.exception(e)

    async def send_attachment(self, sender_id, url_or_id):
        try:
            if url_or_id.startswith('http'):
                data = {
                    'recipient': {
                        'id': f'{sender_id}'
                    },
                    'message': {
                        'attachment': {
                            'type': 'image',
                            'payload': {'is_reusable': True,
                                        'url': url_or_id}
                        }
                    }
                }
                async with self.session.post(static.URL, data=json.dumps(data),
                                             headers={'Content-type': 'application/json'}) as resp:
                    return await resp.json()

            else:
                data = {
                    'recipient': {
                        'id': f'{sender_id}'
                    },
                    'message': {
                        'attachment': {
                            'type': 'image',
                            'payload': {'attachment_id': url_or_id}
                        }
                    }
                }
                async with self.session.post(static.URL, data=json.dumps(data),
                                             headers={'Content-type': 'application/json'}) as resp:
                    return await resp.json()

        except Exception as e:
            error_log.exception(e)


    async def carousel_template(self, sender_id, **kwargs):
        try:
            el = []
            for key, val in kwargs.items():
                elem_list = {
                    'title': f'{val[0]}',
                    'image_url': f'{val[2]}',
                    'subtitle': f'{val[1]}',
                    'default_action': {
                        'type': 'web_url',
                        'url': 'https://abmcloud.com/',
                        "webview_height_ratio": "tall"}
                }
                el.append(elem_list)


            data = {
                "messaging_type": "RESPONSE",
                'recipient': {'id': f'{sender_id}'},
                'message': {
                    'attachment': {
                        'type': 'template',
                        'payload': {
                            'template_type': 'generic',
                            'elements': el}}}}

            async with self.session.post(static.URL,
                                         data=json.dumps(data),
                                         headers={'Content-type': 'application/json'}) as resp:
                return await resp.json()

        except Exception as e:
            error_log.exception(e)
