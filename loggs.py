import logging
import os


'''
class InfoFilter(logging.Filter):
    def filter(self, rec):
        return rec.levelno == logging.INFO

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('FBloylogger')
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(filename)s[LINE: %(lineno)d] - %(asctime)s - %(levelname)s - %(message)s')

handler_error = logging.FileHandler('loybotERROR.log')
handler_error.setLevel(logging.WARNING)
handler_error.setFormatter(formatter)
logger.addHandler(handler_error)


handler_info = logging.FileHandler('loybotINFO.log')
handler_info.setLevel(logging.INFO)
handler_info.setFormatter(formatter)
logger.addHandler(handler_info)
'''


def setup_logger(log_name, log_file, level=logging.INFO):
    l = logging.getLogger(log_name)
    formatter = logging.Formatter('%(filename)s[LINE: %(lineno)d] - %(asctime)s - %(levelname)s - %(message)s')
    file_handler = logging.FileHandler(log_file)
    file_handler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(file_handler)


setup_logger('errorLogger', os.path.abspath('/home/botuser/fbloybot/loybotERROR.log'))
setup_logger('infoLogger', os.path.abspath('/home/botuser/fbloybot/loybotINFO.log'))

errorlog = logging.getLogger('errorLogger')
infolog = logging.getLogger('infoLogger')
