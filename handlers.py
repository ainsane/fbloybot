import loggs
from asyncbot import bot, loyalty, mongo
from asyncio import get_event_loop

loop = get_event_loop()
bot = bot
loyalty = loyalty
mongo = mongo

error_log = loggs.errorlog
info_log = loggs.infolog




#                                       Обработка обычных сообщений
async def handle_simple_message(message_event):
    sender_id = message_event['sender']['id']
    text = message_event['message']['text']
    user = await mongo.find_user(sender_id)
#
#                                Проверка кода из СМС при регистрации/аутентификации
#
    if user is not False and isinstance(user['state'], dict):
        if text.isdigit():
            if 'reg_id' in user['state']:
                auth = await loyalty.auth_registration_confirm(int(user['state']['reg_id']), text)
                print(auth)
                if 'token' in auth['data']:
                    token = auth["data"]["token"]
                    fields = {'token': token,
                              'state': 'ready'}
                    await mongo.write_field(sender_id, **fields)
                    await bot.send_text_message(sender_id, 'Регистрация пройдена!'
                                                           '\nГенерирую штрих-код и qr-код!)')
                    data = {
                        'sender': {'id': sender_id},
                        'postback': {'payload': 'codebar'}
                    }
                    await handle_postbacks(data)
                    data = {
                        'sender': {'id': sender_id},
                        'postback': {'payload': 'codeqr'}
                    }
                    await handle_postbacks(data)

                else:
                    buttons = {'Регистрация': 'registration'}
                    await bot.send_quick_reply_text(sender_id, 'Что-то пошло не так...\nПоробуйте вести еще раз', **buttons)

            elif 'auth_id' in user['state']:
                sms_id = user['state']['auth_id']
                auth = await loyalty.auth_phone_confirm(int(sms_id), str(text))
                print(auth)
                if 'token' in auth['data']:
                    token = auth['data']['token']
                    fields = {
                        'token': token,
                        'state': 'ready'
                    }
                    await mongo.write_field(sender_id, **fields)
                    await bot.send_text_message(sender_id, 'Аутентификация успешна!'
                                                           '\nВаш штрих-код и qr-код:')
                    data = {
                        'sender': {'id': sender_id},
                        'postback': {'payload': 'codebar'}
                    }
                    await handle_postbacks(data)
                    data = {
                        'sender': {'id': sender_id},
                        'postback': {'payload': 'codeqr'}
                    }
                    await handle_postbacks(data)
                else:
                    buttons = {'Заново отправить смс': 'resend_sms'}
                    await bot.send_quick_reply_text(sender_id, 'Что-то пошло не так...\nПоробуйте ввести еще раз', **buttons)

        elif not text.isdigit():
            buttons = {'Заново отправить смс': 'resend_sms'}
            await bot.send_quick_reply_text(sender_id, 'Пароль должен состоять из цифр!\nПопробуйте еще раз', **buttons)
            #await bot.send_text_message(sender_id, '')


#                                     Если стейт nophone и человек что то написал - отправляем на регистрацию
#
    elif user is not False and 'nophone' in user['state']:
        await bot.quick_reply_phone(sender_id, f'Вы не зарегестрированы в системе!\n'
                                               f'Для начала работы поделись номером телефона)')

#
#                    Редактируем данные:
#
    elif user is not False and user['state'] in ['first_name', 'last_name', 'birth_day', 'email']:
        print('heREER')
        token = user['token']
        edit_data = {user['state']: text}
        edit_profile = await loyalty.edit_profile(token, edit_data)
        if edit_profile == 200:
            post_data = {
                'sender': {'id': sender_id},
                'postback': {'payload': 'profile'}
            }
            await handle_postbacks(post_data)
            await mongo.set_state(sender_id, 'ready')
        elif edit_profile == 422:
            await bot.send_text_message(sender_id, 'Неверный формат даты\n'
                                                   'Пример: 1986-06-28')
        else:
            print(edit_profile)

    elif user is False or user:
        await bot.send_text_message(sender_id, text)
        print(message_event['message'].get('quick_reply'))
        print(sender_id, text)


#                                                 Обработка сообщений quick_reply
async def handle_quick_reply(message_event):
    sender_id = message_event['sender']['id']
    quick_payload = message_event['message']['quick_reply']['payload']
    user = await mongo.find_user(sender_id)
    print(message_event)

#                                       Получаем НОМЕР ТЕЛЕФОНА
#
    if quick_payload.startswith('+'):
        phone_num = quick_payload[1:]
        await mongo.write_phone(sender_id, phone_num, 'phone')
        try:
            auth = await loyalty.auth_phone(phone_num)
            if auth == 'notreg':
                buttons = {'Регистрация': 'registration'}
                await bot.send_quick_reply_text(sender_id, 'Вы еще не зарегестрированы в системе', **buttons)
                await mongo.set_state(sender_id, 'notreg')

            elif auth[0] == 'ok':
                sms_id = int(auth[1])
                print(sms_id)
                await mongo.set_state(sender_id, {'auth_id': sms_id})
                await bot.send_text_message(sender_id, 'Введите код из SMS сообщения')

        except Exception as e:
            print(e)

    elif quick_payload == 'registration':
        reg = await loyalty.registration(user['phone'])
        if reg[0] == 'ok':
            await bot.send_text_message(sender_id, 'Введите код из SMS сообщения')
            print(reg[1])
            sms_id = reg[1]
            data = {'state': {'reg_id': sms_id}}
            await mongo.write_field(sender_id, **data)
        elif reg[0] == 'error1':
            msg = reg[1]['data']['message']
            await bot.send_text_message(sender_id, str(msg))

#                                   Пересылаем смс, (слздаём месседж ивент и передаём в функцию quick reply)


    elif quick_payload == 'resend_sms':
        send_mess_event = {
            'sender': {'id': sender_id},
            'message': {'quick_reply': {'payload': f'+{user["phone"]}'}}
        }
        await handle_quick_reply(send_mess_event)

    elif quick_payload == 'edit_profile':
        token = user['token']
        profile_cards = await loyalty.profile(token)
        if profile_cards is not False and profile_cards[0] == 'ok':
            birth_day = profile_cards[1]['birth_day']
            if birth_day is None:


                edit_buttons = {
                    'Имя': 'first_name',
                    'Фамилия': 'last_name',
                    'Дата рождения': 'birth_day',
                    'E-mail': 'email'
                }
                await bot.send_quick_reply_text(sender_id, 'Выбери параметр для редактирования:', **edit_buttons)
            else:
                edit_buttons = {
                    'Имя': 'first_name',
                    'Фамилия': 'last_name',
                    'E-mail': 'email'
                }
                await bot.send_quick_reply_text(sender_id, 'Выбери параметр для редактирования:', **edit_buttons)

    elif quick_payload in ['first_name', 'last_name', 'birth_day', 'email']:
        if quick_payload == 'birth_day':
            await bot.send_text_message(sender_id, 'Внимание!\nЗадать дату рождения можно только один раз!\n'
                                                   'Напиши дату рождения в формате: 1986-06-28')
            await mongo.set_state(sender_id, quick_payload)
        else:
            naming = {
                'first_name': 'Имя',
                'last_name': 'Фамилию',
                'email': 'E-mail'
            }
            await bot.send_text_message(sender_id, f'Редактируем {naming[quick_payload]}\n'
                                                   f'Просто напиши мне новый вариант)')
            await mongo.set_state(sender_id, quick_payload)

    if quick_payload.startswith('id'):
        token = user['token']
        purchases = await loyalty.list_purchases(token)
        purch = purchases['data']['purchases']
        purch_id = quick_payload[2:]
        for item in purch:
            if item['transactions'][0]['processing_transaction_id'] == int(purch_id):
                pos = item['transactions'][0]['check_positions']
                def values(p):
                    prod_name = p['product_name']
                    price = p['product_price']
                    amount = p['product_amount']
                    prod_sum = p['product_sum']

                    return f'\U0001F680Товар: {prod_name}\n' \
                           f'  Цена: {price}грн\n' \
                           f'  Кол-во: {amount}\n' \
                           f'  Сумма: {prod_sum}грн\n\n'

                s = ''.join(map(values, pos))
                summ = item['transactions'][0]['pay_sum']
                bon = item['transactions'][0]['pay_bonus']

                await bot.send_text_message(sender_id, f'{s}Итого: {summ}грн({bon}BON)')

    if quick_payload.startswith('next'):
        token = user['token']
        purchases = await loyalty.list_purchases(token)
        purch = purchases['data']['purchases']

        if purchases is not False:
            buttons = {}

            def values(num, p):
                
                numb = num[0]
                shop = p['transactions'][0]['shop']
                paysum = p['transactions'][0]['pay_sum']
                paybonus = p['transactions'][0]['pay_bonus']
                paydate = p['transactions'][0]['formatted_pay_date']
                paytime = p['transactions'][0]['formatted_pay_time']
                trans_item = p['transactions'][0]['processing_transaction_id']
                
                buttons[f'  {str(numb)}'] = f'id{str(trans_item)}'

                return f'{numb}\u20E3\n \U0001F4CD{shop}\n' \
                       f'\U0001F4B8 {paysum}грн ({paybonus}BON)\n' \
                       f'\U0001F4C5{paydate}({paytime})\n\n'

            if quick_payload == 'next1':
                if len(purch) > 10:
                    buttons['ещё'] = 'next2'
                s = ''.join(map(values, enumerate(purch, 1), purch[5:10]))

                text = s + '\nВыбери номер покупки, чтобы просмотреть детали:'

                await bot.send_quick_reply_text(sender_id, text, **buttons)

            if quick_payload == 'next2':
                if len(purch) > 15:
                    buttons['ещё'] = 'next3'
                s = ''.join(map(values, enumerate(purch, 1), purch[10:15]))
                text = s + '\nВыбери номер покупки, чтобы просмотреть детали:'

                await bot.send_quick_reply_text(sender_id, text, **buttons)

            if quick_payload == 'next3':
                s = ''.join(map(values, enumerate(purch, 1), purch[15:20]))
                text = s + '\nВыбери номер покупки, чтобы просмотреть детали:'

                await bot.send_quick_reply_text(sender_id, text, **buttons)

    


#                                          Обработка Postback`ов
async def handle_postbacks(message_event):
    sender_id = message_event['sender']['id']
#    recipient_id = message_event['recipient']['id']
    postback_payload = message_event['postback']['payload']
    user = await mongo.find_user(sender_id)

    #                               Кнопка "НАЧАТЬ" или "Перезапустить бота"
    if postback_payload == 'GET_STARTED_PAYLOAD':
        if user is False:
            await bot.quick_reply_phone(sender_id, f'Привет!\n'
                                                     f'Для начала работы поделись номером телефона)')
            await mongo.write_user(sender_id, 'nophone')

        elif user['state'] == 'nophone':
            await bot.quick_reply_phone(sender_id, f'Привет!\n'
                                                     f'Для начала работы поделись номером телефона)')

        elif user['state'] == 'phone':
            phone_num = user['phone']

            #           Проверка телефона в системе
            auth = await loyalty.auth_phone(phone_num)
            if auth == 'notreg':
                buttons = {'Регистрация': 'registration'}
                await bot.send_quick_reply_text(sender_id, 'Вы еще не зарегестрированы в системе', **buttons)
                await mongo.set_state(sender_id, 'notreg')

            elif auth[0] == 'ok':
                sms_id = int(auth[1])
                print(sms_id)
                await mongo.set_state(sender_id, {'auth_id': sms_id})
                await bot.send_text_message(sender_id, 'Введите код из SMS сообщения')

        elif user['state'] == 'notreg':
            buttons = {'Регистрация': 'registration'}
            await bot.send_quick_reply_text(sender_id, 'Вы еще не зарегестрированы в системе', **buttons)

        elif isinstance(user['state'], dict):
            buttons = {'Заново отправить смс': 'resend_sms'}
            await bot.send_quick_reply_text(sender_id, 'Введите код из SMS сообщения', **buttons)

        elif user['state'] == 'ready':
            await bot.send_text_message(sender_id, 'Добро пожаловать в ABM Loyalty')

    elif user is False:
        await bot.quick_reply_phone(sender_id, f'Привет!\n'
                                                 f'Для начала работы поделись номером телефона)')
        await mongo.write_user(sender_id, 'nophone')

    elif user['state'] == 'nophone':
        await bot.quick_reply_phone(sender_id, f'Привет!\n'
                                               f'Для начала работы поделись номером телефона)')
    elif user['state'] == 'notreg':
        buttons = {'Регистрация': 'registration'}
        await bot.send_quick_reply_text(sender_id, 'Вы еще не зарегестрированы в системе', **buttons)

    elif isinstance(user['state'], dict):
        buttons = {'Заново отправить смс': 'resend_sms'}
        await bot.send_quick_reply_text(sender_id, 'Введите код из SMS сообщения', **buttons)

    elif postback_payload == 'balance' and user['state'] == 'ready':
        token = user['token']
        bal = await loyalty.balance(token)
        if bal[0] == 'ok':
            bonbal = bal[1]
            available = bal[2]
            await bot.send_text_message(sender_id, f'Баланс: {bonbal}'
                                                   f'\nДоступно: {available}')
        else:
            data = bal[1]
            error_log.error(data)
            await bot.send_text_message(sender_id, 'Невозможно получить информацию.')



    elif postback_payload.startswith('code'):
        token = user['token']
        if postback_payload == 'codebar':
            if 'bar_code' not in user:
                bar = await loyalty.barcode(token)
                if bar is not False:
                    barcode_url = bar['data']['bar_code']
                    res = await bot.send_attachment(sender_id, barcode_url)
                    attach_id = res['attachment_id']
                    fields = {
                        'bar_code': attach_id
                    }
                    await mongo.write_field(sender_id, **fields)
                else:
                    await bot.send_text_message(sender_id, 'Невозможно получить информацию.')
            else:
                attach_id = user['bar_code']
                await bot.send_attachment(sender_id, attach_id)

        elif postback_payload == 'codeqr':
            if 'qr_code' not in user:
                qr = await loyalty.qrcode(token)
                if qr is not False:
                    qr_url = qr['data']['qr_code']
                    res = await bot.send_attachment(sender_id, qr_url)
                    attach_id = res['attachment_id']
                    fields = {
                        'qr_code': attach_id
                    }
                    await mongo.write_field(sender_id, **fields)
                else:
                    await bot.send_text_message(sender_id, 'Невозможно получить информацию.')
            else:
                attach_id = user['qr_code']
                await bot.send_attachment(sender_id, attach_id)

    elif postback_payload == 'profile':
        token = user['token']
        profile_cards = await loyalty.profile(token)
        if profile_cards is not False and profile_cards[0] == 'ok':
            mobile = profile_cards[1]['mobile']
            email = profile_cards[1]['email']
            first_name = profile_cards[1]['first_name']
            last_name = profile_cards[1]['last_name']
            birth_day = profile_cards[1]['birth_day']
            buttons = {'Редактировать данные': 'edit_profile'}
            await bot.send_quick_reply_text(sender_id, f'Имя: {first_name}\n'
                                                   f'Фамилия: {last_name}\n'
                                                   f'Дата рождения: {birth_day}\n'
                                                   f'Номер телефона: {mobile}\n'
                                                   f'E-mail: {email}\n'
                                                   f'№ карты:{profile_cards[2]}',
                                            **buttons)

        else:
            await bot.send_text_message(sender_id, 'Ошибка\nНевозможно получить информацию.')
    elif postback_payload == 'purchases':
        #print('ok')
        token = user['token']
        purchases = await loyalty.list_purchases(token)
        if purchases is not False:
            purch = purchases['data']['purchases']
            buttons = {}
            def values(num, p):
                                
                numb = num[0]
                shop = p['transactions'][0]['shop']
                paysum = p['transactions'][0]['pay_sum']
                paybonus = p['transactions'][0]['pay_bonus']
                paydate = p['transactions'][0]['formatted_pay_date']
                paytime = p['transactions'][0]['formatted_pay_time']
                trans_item = p['transactions'][0]['processing_transaction_id']
                
                buttons[f'  {str(numb)}'] = f'id{str(trans_item)}'

                if numb <= 9:
                    return f'{numb}\u20E3\n \U0001F4CD{shop}\n' \
                           f'\U0001F4B8 {paysum}грн ({paybonus}BON)\n' \
                           f'\U0001F4C5{paydate}({paytime})\n\n'
                else:
                    return f'{1}\u20E3{0}\u20E3\n \U0001F4CD{shop}\n' \
                           f'\U0001F4B8 {paysum}грн ({paybonus}BON)\n' \
                           f'\U0001F4C5{paydate}({paytime})\n\n' \
                           f'Выберите номер покупки чтобы просмотреть детали:'
            if len(purch) > 5:
                buttons['ещё'] = 'next1'

            s = ''.join(map(values, enumerate(purch, 1), purch[:5]))
            text = s + '\nВыбери номер покупки, чтобы просмотреть детали:'

            await bot.send_quick_reply_text(sender_id, text, **buttons)

    elif postback_payload == 'news':
        token = user['token']
        news = await loyalty.news(token)
        if news is not False:
            buttons = {}
            for key in news['data']['items']:
                item_id = str(key['id'])
                name = key['name']
                desc = key['description_short']
                link = key['img_path']

                buttons[item_id] = [name, desc, link]

            await bot.carousel_template(sender_id, **buttons)

    elif postback_payload == 'actions':
        token = user['token']
        actions = await loyalty.actions(token)
        if actions is not False:
            print(actions['data'])
            buttons = {}
            for key in actions['data']['items']:
                item_id = str(key['id'])
                name = key['title']
                desc = key['content']
                link = key['img_path']

                buttons[item_id] = [name, desc, link]

            await bot.carousel_template(sender_id, **buttons)

