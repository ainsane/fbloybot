
import aiohttp
import loggs

error_log = loggs.errorlog
info_log = loggs.infolog
# API   LOYALTY


class LoyAPI:

    def __init__(self, loop):
        self.session = aiohttp.ClientSession(loop=loop)

    async def close(self):
        await self.session.close()


    async def auth_phone(self, phone_num):
        try:
            async with self.session.post('https://api.m-reward.com/v2.1/client/auth-phone',
                                         data={'phone': str(phone_num)}) as resp:
                if resp.status == 201:
                    sms = await resp.json()
                    sms_id = sms['data']['sms_id']
                    info_log.info(sms)
                    return ['ok', sms_id]

                elif resp.status == 422:
                    resp = await resp.json()
                    # доделать обработчик других вариантов которые спровоцированы 422
                    info_log.info(resp)
                    return 'notreg'

        except Exception as e:
            error_log.exception(e)


    async def auth_phone_confirm(self, sms_id, str_sms_pass):
        try:
            async with self.session.post('https://api.m-reward.com/v2.1/client/auth-phone-confirm',
                                         data={'code': f'{str_sms_pass}', 'sms_id': sms_id}) as resp:

                data = await resp.json()
                info_log.info(data)
                return data

        except Exception as e:
            error_log.exception(e)

    async def registration(self, phone_num):
        try:
            async with self.session.post('https://api.m-reward.com/v2.1/client/registration',
                                         data={'phone': phone_num}) as resp:
                reg = await resp.json()
                info_log.info(reg)
                if resp.status == 201:
                    sms_id = reg['data']['sms_id']
                    return ['ok', sms_id]
                elif resp.status == 422:
                    return ['error1', reg]
                else:
                    return ['error', reg]
        except Exception as e:
            error_log.exception(e)

    async def auth_registration_confirm(self, sms_id, str_sms_pass):
        try:
            async with self.session.post('https://api.m-reward.com/v2.1/client/registration-confirm',
                                         data={'code': f'{str_sms_pass}', 'sms_id': sms_id}) as res:
                data = await res.json()
                info_log.info(data)
                return data
        except Exception as e:
            error_log.exception(e)

    async def balance(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.get('https://api.m-reward.com/v2/client/account/BON/balance', auth=auth)as resp:
                data = await resp.json()
                info_log.info(data)
                if resp.status == 200:
                    bal = data['data']['balance']
                    available = data['data']['balance']
                    return ['ok', bal, available]
                else:
                    return ['not_ok', data]
        except Exception as e:
            error_log.exception(e)



    async def barcode(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.get('https://api.m-reward.com/v2/client/bar-code', auth=auth) as resp:
                data = await resp.json()
                info_log.info(data)
                if resp.status == 200:
                    return data
                else:
                    error_log.error(data)
                    return False
        except Exception as e:
            error_log.exception(e)


    async def qrcode(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.get('https://api.m-reward.com/v2/client/qr-code', auth=auth) as resp:
                data = await resp.json()
                info_log.info(data)
                if resp.status == 200:
                    return data
                else:
                    error_log.error(data)
                    return False
        except Exception as e:
            error_log.exception(e)


    async def profile(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)

            async with self.session.get('https://api.m-reward.com/v2/client/profile', auth=auth) as profile:

                async with self.session.get('https://api.m-reward.com/v2/client/card/card-info', auth=auth) as card:

                    if profile.status == 200 and card.status == 200:
                        prof_json = await profile.json()
                        card_json = await card.json()

                        cards = ''
                        for c in card_json['data']['cards']:
                            if c['number'].startswith('200'):
                                cards += f'\n{c["number"]} (virtual)'
                            else:
                                cards += f'\n{c["number"]}'

                        return ['ok', prof_json['data'], cards]
                    else:
                        return False

        except Exception as e:
            error_log.exception(e)


    async def edit_profile(self,token, edit_data):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.put('https://api.m-reward.com/v2/client/profile',
                                        data=edit_data, auth=auth) as resp:
                if resp.status == 200:
                    return 200
                elif resp.status == 422:
                    return 422
                else:
                    error_log.error(await resp.json())
                    return False
        except Exception as e:
            error_log.exception(e)

    async def list_purchases(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.get('https://api.m-reward.com/v2/client/purchase-history',
                                        auth=auth) as resp:
                resp_json = await resp.json()
                if resp.status == 200:
                    return resp_json
                else:
                    error_log.error(resp_json)
                    return False
        except Exception as e:
            error_log.exception(e)

    async def news(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.get('https://api.m-reward.com/v2/client/partner/news-all',
                                        auth=auth) as resp:
                resp_json = await resp.json()
                if resp.status == 200:
                    return resp_json
                else:
                    error_log.error(resp_json)
                    return False
        except Exception as e:
            error_log.exception(e)

    async def actions(self, token):
        try:
            auth = aiohttp.BasicAuth(login=token)
            async with self.session.get('https://api.m-reward.com/v2/client/partner/actions',
                                        auth=auth) as resp:
                resp_json = await resp.json()
                if resp.status == 200:
                    return resp_json
                else:
                    error_log.error(resp_json)
                    return False
        except Exception as e:
            error_log.exception(e)

